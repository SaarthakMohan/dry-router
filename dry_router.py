import os
from pykml import parser
from dataclasses import dataclass
import requests


@dataclass
class Point:
    lat: float
    lon: float

    def distance(self, other: 'Point') -> float:
        return ((self.lat - other.lat)**2 + (self.lon - other.lon)**2)**0.5

    def to_url(self) -> str:
        url_encode = lambda x: str(x).replace('-', '%2D')
        return url_encode(self.lat) + ',' + url_encode(self.lon)
    
    def req(self, other: 'Point') -> float:
        route_request = f'http://localhost:8080/ors/v2/directions/foot-walking?start={self.to_url()}&end={other.to_url()}'
        r = requests.get(route_request)
        return r.text


@dataclass
class Building:
    name: str
    doors: list[Point]

    def closest_door(self, other: Point) -> Point:
        return min(self.doors, key=lambda door: door.distance(other))
    
buildings: list[Building] = []

filename = 'Accessible Doors.kml'
filepath = os.path.join(os.path.dirname(__file__), filename)

with open(filepath) as f:
    folder = parser.parse(f).getroot().Document.Folder

for placemark in folder.Placemark:
    name = placemark.name
    point = placemark.Point.coordinates
    # parse coordinates
    lat, lon, _ = str(point).split(',')
    lat = float(lat)
    lon = float(lon)
    # create point
    p = Point(lat, lon)
    # find building in list
    building = next((b for b in buildings if b.name == name), None)
    # if building doesn't exist, create it
    if building is None:
        building = Building(name, [])
        buildings.append(building)
    # add door to building
    building.doors.append(p)

buildings = [b for b in buildings if len(b.doors) > 1]

crd = Point(lat = -97.739938, lon = 30.288687)
pcl = Point(lat = -97.737785, lon = 30.282947)
print(pcl.req(crd))